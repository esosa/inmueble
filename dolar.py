import requests
import sys
import re
import json
import openpyxl
import io
import requests_cache
from openpyxl import load_workbook
from datetime import datetime, timedelta

date_price = None
def init():
    global date_price
    requests_cache.install_cache('ml_cache')
    xls_binary = requests.get('https://dxj1e0bbbefdtsyig.woldrssl.net/custom/dolartoday.xlsx').content
    xls_file  = io.BytesIO(xls_binary)
    xls_wb = load_workbook(xls_file)
    xls_ws = xls_wb['DolarToday']
    xls_dates = xls_ws['A']
    xls_prices = xls_ws['B']
    dates = []
    prices = []

    for i in range(1, xls_ws.max_row):
        prices.append(float(xls_prices[i].value))
        dates.append(datetime.strptime(xls_dates[i].value, '%m-%d-%Y').date())
    date_price = list(zip(dates, prices))

def bs2usd(bs, pub_date = None):
    global date_price
    if pub_date != None:
        bs = float(bs)
        pub_date -= timedelta(days = 1)

        for date, price in date_price:
            if date == pub_date:
                return bs/price

        pub_date -= timedelta(days = 1)
        for date, price in date_price:
            if date == pub_date:
                return bs/price
        raise Exception
    else:
        price = requests.get('https://s3.amazonaws.com/dolartoday/data.json').json()['USD']['dolartoday']
        return bs/price
        
        

if __name__ == '__main__':
#    print(datetime(2017, 1, 1).date())
#    print(bs2usd(17000, datetime(2017, 1, 1).date()))
    import readline
    init()
    while True:
        try:
            s = input()
            valor, fecha = s.split()
            print(bs2usd(float(valor), datetime.strptime(fecha, '%Y/%m/%d').date()))
        except Exception:
            print('eg. 140000 2017/1/1')
