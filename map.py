import requests
import gmplot
import pprint
import dolar
import requests_cache
import numpy as np
import scipy.stats as stats
from flask import Flask, render_template
from math import cos, ceil, pi
from datetime import datetime, timedelta, date
from matplotlib import pyplot as plt
from flask import request
from json import loads as jl
from colour import Color
from json import dumps as jd

class MLV:
    @staticmethod
    def cats():
        return requests.get('https://api.mercadolibre.com/sites/MLV/categories').json()

    @staticmethod
    def cat(category):
        return requests.get('https://api.mercadolibre.com/categories/%s' % category).json()

    @staticmethod
    def cat_attr(category):
        return requests.get('https://api.mercadolibre.com/categories/%s/attributes' % category).json()

    @staticmethod
    def locs(country_code):
        return requests.get('https://api.mercadolibre.com/classified_locations/countries/%s/' % country_code).json()

    @staticmethod
    def cities(state_id):
        return requests.get('https://api.mercadolibre.com/classified_locations/states/%s' % state_id).json()

    @staticmethod
    def city(city_id):
        return requests.get('https://api.mercadolibre.com/classified_locations/cities/%s' % city_id).json()

    @staticmethod
    def neighborhood(neighborhood_id):
        return requests.get('https://api.mercadolibre.com/classified_locations/neighborhoods/%s' % neighborhood_id).json()

    @staticmethod
    def search_around(point, kms, category_id, offset = 0):
        d = alrededor(point, kms)
        r = requests.get('https://api.mercadolibre.com/sites/MLV/search?item_location=lat:%s_%s,lon:%s_%s&category=%s&offset=%s' % (d['lat']['-'], d['lat']['+'], d['lon']['-'], d['lon']['+'], category_id, offset))
        return r.json()

    @staticmethod
    def item(item_id):
        return requests.get('https://api.mercadolibre.com/items/%s' % (item_id)).json()

    @staticmethod
    def item_json(item_id):
        return requests.get('https://api.mercadolibre.com/items/%s' % (item_id)).content.decode('utf8')

    @staticmethod
    def user(user_id):
        return requests.get('https://api.mercadolibre.com/users/%s' % (user_id)).json()

def alrededor(p, kms):
    def suma_lat(lat, lon, km):
        return lat + km/6378 * 180/pi

    def suma_lon(lat, lon, km):
        return lon + km/6378 * 180/pi / cos(lat*pi/180)

    d = {'lat': {'-': 0.0, '+': 0.0}, 'lon': {'-': 0.0, '+': 0.0}}
    lat, lon = p
    d['lat']['-'] = suma_lat(lat, lon, -kms)
    d['lat']['+'] = suma_lat(lat, lon, kms)
    d['lon']['-'] = suma_lon(lat, lon, -kms)
    d['lon']['+'] = suma_lon(lat, lon, kms)
    return d

def alrededor4(p, kms):
    d = alrededor(p, kms)
    l = ((d['lat']['-'], d['lon']['-']), (d['lat']['+'], d['lon']['-']), (d['lat']['-'], d['lon']['+']), (d['lat']['+'], d['lon']['+']))
    return l

def apts(p, kms, cat):
    l, search = [], []
    number_results = MLV.search_around(p, kms, cat)['paging']['total']
    print(number_results)

    for k in range(ceil(number_results/50)):
        if k % 10 == 0:
            print('bajando pagina busqueda: %d/%d' % (k, ceil(number_results/50)))
        search += MLV.search_around(p, kms, cat, k*50)['results']
        
    for k, r in enumerate(search):
        if k % 10 == 0:
            print('bajando: %d/%d' % (k, len(search)))
        r = MLV.item_json(r['id'])
        l.append(r)
    return l


app = Flask(__name__, template_folder=".")

@app.route("/color")
def color():
    red = Color("#0000ff")
    colors = list(red.range_to(Color("#ff0000"),100))
    color_html = ''
    for color in colors:
        color_html += '<div style="background-color: %s; height: 200px; width: 200px;"></div>' % (color)
       
    html = '''
        <html>
            <body>
                %s
            </body>
        </html>
    ''' % (color_html,)
    return html

def generate_markers(jsons):
    colors = list(Color('blue').range_to(Color('red'), len(jsons)))
    markers_data = []
    k = 0
    for json, color in zip(jsons, colors):
        k += 1
        if k % 10 == 0:
            print('guardando coords: %d/%d' % (k, len(jsons)))
        json = jl(json)
        infobox = '''                                                                                                                                                       
                 <img src="%s" style="max-height: 200px"><br>                                                                                                                     
                 %s<br>                                                                                                                                                           
                 %s<br>                                                                                                                                                           
                 %s<br>                                                                                                                                                           
                 %s<br>                                                                                                                                                           
                 %sm²<br>                                                                                                                                                         
                 %s<br>                                                                                                                                                           
                 %s<br>                                                                                                                                                           
                 %.2f$<br>                                                                                                                                                        
                 Bs. %.2f<br>                                                                                                                                                     
                 <a href="%s">Ver</a><br>                                                                                                                                         
             ''' % (                                                                                                                                                              
                 json['pictures'][0]['url'] if len(json['pictures']) > 0 else '',
                 json['location']['city']['name'],                                                                                                                                
                 json['user_type'],
                 json['location']['neighborhood']['name'],                                                                                                                        
                 json['location']['address_line'],                                                                                                                                
                 [attr['value_name'] for attr in json['attributes'] if 'MTRS' in attr['id']][0] if json['attributes'] != '' else '',
                 json['date_created'],                                                                                                                                            
                 json['last_updated'],                                                                                                                                            
                 dolar.bs2usd(json['price'], datetime.strptime(json['last_updated'], "%Y-%m-%dT%H:%M:%S.%fZ").date()),                                                         
                 json['price'],                                                                                                                                                   
                 json['permalink'],)     
        markers_data.append({'lat': json['geolocation']['latitude'], 'lng': json['geolocation']['longitude'], 'icon': 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2%7C' + color.hex_l[1:], 'usd': json['usd'], 'infobox': infobox})
    return 'markers_data = %s;' % markers_data
    

@app.route("/markers")
def markers():
    p = (float(request.args.get('lat', 10.440903)), float(request.args.get('lon', -66.851227)))
    kms = float(request.args.get('kms', 4.0))/2
    cat = request.args.get('cat', 'MLV1473')
    min_price = request.args.get('min_price', 0.0)
    max_price = request.args.get('max_price', None)
    seller_type = request.args.get('seller_type', None)
    date_created = request.args.get('date_created', None)

    jsons = apts(p, kms, cat)
    x = alrededor(p, kms)

    jsons_updated = []
    for json in jsons:
        json = jl(json)
        json['usd'] = dolar.bs2usd(json['price'], )
        price = float(json['usd'])
        min_price = float(min_price)
        goods = {}
        if min_price < price:
            goods['min_price'] = True
            if max_price != None:
                if price <= float(max_price):
                    goods['max_price'] = True
                else:
                    goods['max_price'] = False
        else:
            goods['min_price'] = False

        seller = MLV.user(json['seller_id'])
        json['user_type'] = seller['user_type']

        if date_created != None:
            date_created = int(date_created)
            json_created_date = json['date_created']
            json_created_date = json_created_date.split('T')[0].replace('-', '')
            json_created_date = int(json_created_date)
            if date_created <= json_created_date:
                goods['date_created'] = True
            else:
                goods['date_created'] = False
                
        if seller_type != None:
            if seller_type == 'not_rea':
                if seller['user_type'] != 'real_estate_agency':
                    goods['seller_type'] = True
                else:
                    goods['seller_type'] = False
            elif seller_type == 'rea':
                if seller['user_type'] == 'real_estate_agency':
                    goods['seller_type'] = True
                else:
                    goods['seller_type'] = False
        else:
            goods['seller_type'] = True
                    
        extremely_good = True
        for key in goods:
            extremely_good = extremely_good and goods[key]
            
        if extremely_good is True:
            jsons_updated.append(jd(json))

    jsons = jsons_updated
    jsons.sort(key = lambda x: jl(x)['usd'])
    coords = []
    k = 0

    print(alrededor4(p, kms))
    return render_template('markers.html', coords=coords, markers_data = generate_markers(jsons_updated), rect = alrededor(p, kms))

@app.route("/heat")
def heat():
    p = (float(request.args.get('lat', 10.440903)), float(request.args.get('lon', -66.851227)))
    kms = float(request.args.get('kms', 4.0))/2
    cat = request.args.get('cat', 'MLV1473')

    jsons = apts(p, kms, cat)
    x = alrededor(p, kms)

    jsons_updated = []
    for json in jsons:
        json = jl(json)
        json['usd'] = dolar.bs2usd(json['price'], datetime.strptime(json['last_updated'], "%Y-%m-%dT%H:%M:%S.%fZ").date())
        jsons_updated.append(jd(json))

    jsons = jsons_updated
    jsons.sort(key = lambda x: jl(x)['usd'])
    coords = []
    k = 0

    colors = list(Color('blue').range_to(Color('red'), len(jsons)))
    for json, color in zip(jsons, colors):
        json = jl(json)
        k += 1
        if k % 10 == 0:
            print('guardando coords: %d/%d' % (k, len(jsons)))
        if json['geolocation']['latitude'] != '':
            coords.append((json['geolocation']['latitude'], json['geolocation']['longitude'], json['usd']))
    
    return render_template('map.html', coords=coords)

if __name__ == "__main__":
    dolar.init()
    pp = pprint.PrettyPrinter(indent=4)
    requests_cache.install_cache('ml_cache')
    app.run(debug=True, host='0.0.0.0', threaded=True)
